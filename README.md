# Stori-test
Stori-test is a api REST that was developed by Gerardo Blancas. 

The language that this api is programming is python 3 with the framework Flask and use a data base no relational with the MongoDB engine. The performance of the api is send a mail with the total balance, the transactions per month and the average of amount of the transactions if are credit or debit transactions. 

To execute the Api is necesary install Docker and write the next commands in the terminal.

1. docker build -t stori-test
2. docker run -p 5000:5000 stori-test

## Endpoints

Stori-test have 3 diferent endpoints, where each one has a funcionality.


/csv-creator - The first endpoint create the .csv file. Is a get method and the parameters that requires are the next:

### Parameters
name:tranx.csv  The name of the file.
amount_ids:100  The amount of transactions.
year_init:2022  The initail year.
year_end:2022   The final year.

### Response

If the performance of the endpoint is correct, the response is:

{
    "result": true
}

But if is incorrect the response is:

{
    "result": true
}


/csv-reader - The second endpoint read the .csv file and save the data in a collection of mongo DB called tranxs. Is a get method and the parameters that requires are the next:

### Parameters
name:tranx.csv  The name of the file.

### Response

If the performance of the endpoint is correct, the response is:

{
    "result": true
}

But if is incorrect the response is:

{
    "result": true
}

/send-mail - The last endpoint calculate the total balance, the amount of transactions per month and the average of the debit and credit amount. Is a post method and requires a json with the next parameters:

### Parameters
{
    "from": "gerbh25@gmail.com",
    "to": "gerbh25@gmail.com",
    "psd": "**********",
    "cc": " ",
    "subject": "Summary Balance"
}

### Response

If the performance of the endpoint is correct, the response is:

{
    "result": true
}

But if is incorrect the response is:

{
    "result": true
}