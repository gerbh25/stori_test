from csv_creator import csv_generator
from csv_reader_mongo import csv_reader
from email_client import send_email
from mongo_conections import mongo_conections
from flask import Flask
from flask import request


app = Flask(__name__)

@app.route("/")
def hello_world():
    return {"app": "hello world", "by": "Gerardo Blancas"}

@app.route("/csv-creator", methods=["GET"])
def csv_creator():
    requestArgs = request.args
    options = {
    "name": requestArgs["name"],
    "amount_ids": int(requestArgs["amount_ids"]),
    "year_init": int(requestArgs["year_init"]),
    "year_end": int(requestArgs["year_end"])
    }
    try: 
        csv_generator(options)
        return {"result": True}
    except Exception as e:
        raise {"result": False}

@app.route("/csv-reader", methods=["GET"])
def csv_reader_mongo():
    requestArgs = request.args
    mongo_client = mongo_conections()
    csv_file = requestArgs["name"]
    try:
        csv_reader(mongo_client, csv_file)
        return {"result": True}
    except Exception as e:
        raise {"result": False}

@app.route("/send-mail", methods=["POST"])
def send_mail():
    requestArgs = request.get_json()
    try:
        send_email(requestArgs).send()
        return {"result": True}
    except Exception as e:
        raise {"result": False}

if __name__ == '__main__':
    app.run(port=5000, debug=True, host="0.0.0.0")



    
    