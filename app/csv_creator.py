from random import randint, random, choice

class csv_generator():
    def __init__(self, options):
        self.name = options["name"]
        self.amount_ids = options["amount_ids"]
        self.year_init = options["year_init"]
        self.year_end = options["year_end"]

        self.csv_creator()

    def date_generator(self):
        month = randint(1, 12)
        year = randint(self.year_init, self.year_end)
        return f"{month}/{year}"

    def tranx_generator(self):
        amount = round(random() * 100, 2)
        choices = ["+", "-"]
        debit_credit = choice(choices)
        return f"{debit_credit}{amount}"

    def csv_creator(self):
        filename = open(self.name, "w", encoding="utf-8")
        filename.write("id,date,tranx\n")
        for i in range(self.amount_ids):
            id = i + 1
            date = self.date_generator()
            tranx = self.tranx_generator()
            filename.write(f"{id},{date},{tranx}\n")
