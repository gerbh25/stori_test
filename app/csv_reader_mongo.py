from mongo_conections import mongo_conections
from datetime import datetime


class csv_reader():
    def __init__(self, mongo_client, csv_file):
        self.mongo_client = mongo_client
        self.csv_file = csv_file

        insert_json = self.csv_json()
        inserted_ids = self.json_mongo(insert_json)

    def formater_date(self, new_date):
        format_data = "%m/%Y"
        result_date = datetime.strptime(new_date, format_data)
        return result_date

    def csv_json(self):
        with open(self.csv_file, "r") as file:
            lines = file.readlines()
            id = lines[0].split(",")[0]
            date = lines[0].split(",")[1]
            tranx = lines[0].split(",")[2].strip("\n")
        result = []
        for line in lines[1:]:
            dict_result = {}
            dict_result[id] = line.split(",")[0]
            # dict_result[date] = self.formater_date(line.split(",")[1])
            dict_result[date] = line.split(",")[1]
            if "+" in line.split(",")[2]:
                dict_result["type"] = "credit"
                dict_result[tranx] = float(line.split(",")[2].strip("\n").strip("+"))
            elif "-" in line.split(",")[2]:
                dict_result["type"] = "debit"
                dict_result[tranx] = float(line.split(",")[2].strip("\n").strip("-"))
            #dict_result[tranx] = line.split(",")[2].strip("\n")
            result.append(dict_result)
        return result

    def json_mongo(self, register):
        return self.mongo_client.insert_tranxs(register)