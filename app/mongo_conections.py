from pymongo import MongoClient

class mongo_conections():
    def __init__(self):
        self.mongo_client = MongoClient(host="localhost", port=27017)
        self.bd = self.mongo_client["test"]
        self.collection = self.bd["tranxs"]

    def find_tranx(self,query):
        return self.collection.find(query)

    def insert_tranxs(self, register):
        return self.collection.insert_many(register)

    def aggregate_tranxs(self, query):
        return self.collection.aggregate(query)
