import smtplib
from email.utils import formatdate
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from mongo_conections import mongo_conections


class send_email():
    def __init__(self, config):
        self.port = 465
        self.smtp_server_domain_name = "smtp.gmail.com"
        self.sender_mail = config["from"]
        self.password = config["psd"]
        self.to = config["to"]
        self.cc = config["cc"]
        self.subject = config["subject"]

    def calculate_summary(self):

        def calculate_total_balance(query):
            find_balance = mongo_client.aggregate_tranxs(query)
            x = 0
            for i in find_balance:
                if i["_id"] == "credit":
                    x += i["sum_type"]
                else:
                    x -= i["sum_type"]

            return {"_id": "total_balance", "amount": round(x, 3)}

        def calculate_balance(query):
            return mongo_client.aggregate_tranxs(query)

        mongo_client = mongo_conections()
        result = []
        query_total_balance = [{"$group": {"_id": "$type", "sum_type": {"$sum": "$tranx"}}}]
        total_balance = calculate_total_balance(query_total_balance)
        result.append(total_balance)
        query_month_balance = [{"$group": {"_id": "$date", "amount_month": {"$sum": 1}}}]
        total__month = calculate_balance(query_month_balance)
        query_avg_type = [{"$group": {"_id": "$type", "amount_avg": {"$avg": "$tranx"}}}]
        total_avg_type = calculate_balance(query_avg_type)
        for i in total__month:
            result.append(i)
        for i in total_avg_type:
            result.append(i)
        
        return result
    
    def month_balance(self, summary):
        x = ""
        for i in summary[1:]:
            if "amount_month" in i:
                x += "Number of transactions in {}: {}<br>".format(i["_id"], i["amount_month"])
        
        return x


    def html_generator(self, summary):
        month_balabce_str = self.month_balance(summary)
        html = f"""\
        <html>
        <head></head>
        <body>
            <p>Hi user!<br><br>
            In this moment we send you a transaction's summary of your count.<br><br>
            Total balance is {summary[0]["amount"]}<br>
            {month_balabce_str}
            Average debit amount: -{round(summary[-2]["amount_avg"], 3)}<br>
            Average credit amount: {round(summary[-1]["amount_avg"], 3)}<br>
            <br>
            <br>
            </p>
            <img src="stori.png" alt="Stori">
        </body>
        </html>
        """
        return html

    def send(self):
        msg = MIMEMultipart('related')
        msg['Date'] = formatdate(localtime=1)
        msg['To'] = self.to
        msg['Cc'] = self.cc
        msg['Subject'] = self.subject

        summary = self.calculate_summary()
        html = self.html_generator(summary)
        part2 = MIMEText(html, 'html')
        msg.attach(part2)

        server = smtplib.SMTP('smtp.gmail.com:587')
        server.ehlo()
        server.starttls()
        server.ehlo()
        server.auth_login()
        # server.login(self.sender_mail, self.password)
        try:
            result = server.sendmail(self.sender_mail, msg['To'], msg.as_string())
            print("\n\nCorreo enviado correctamente")
        except Exception as e:
            print("Error al enviar el correo electronico: {0}".format(str(e)))

        server.quit()
